#!/usr/bin/env python3

from setuptools import find_packages, setup

if __name__ == "__main__":
    setup(
        name="torbot",
        version="0.0.1",
        license="BSD-2-Clause",
        description="Tor's multi-platform bot",
        author="Alexander Færøy",
        author_email="ahf@torproject.org",
        url="https://gitlab.torproject.org/ahf/torbot",
        packages=find_packages("src"),
        package_dir={"": "src"},
        include_package_data=True,
        install_requires=["aiohttp", "click", "irc3", "python-gitlab"],
        tests_require=[],
        classifiers=[],
        entry_points="""
            [console_scripts]
            torbot=torbot.cli:main
        """,
    )

