# Copyright (c) 2020 The Tor Project, inc. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import sys

import click

from torbot.core.configuration import Configuration
from torbot.core.logging import initialize_logging
from torbot.irc import run_bot_forever

@click.group()
@click.pass_context
def cli(context):
    context.ensure_object(dict)

@cli.command()
@click.argument("path")
@click.pass_context
def run(context, path: str):
    config = Configuration.from_config_directory(path)
    initialize_logging(config)
    run_bot_forever(config)

def main():
    cli(obj={})
