# Copyright (c) 2020 The Tor Project, inc. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import ssl

import irc3

class TorBot(irc3.IrcBot):
    def __init__(self, config):
        self._config = config

        bot_config = dict(
            nick=config.irc_client_nickname(),
            username=config.irc_client_username(),
            realname=config.irc_client_realname(),

            host=config.irc_server_hostname(),
            port=config.irc_server_port(),
            ssl=config.irc_server_tls(),

            debug=True,

            ctcp=dict(
                version="",
                userinfo="",
                time="",
            ),

            autojoins=list(map(lambda x: x["channel"],
                               config.irc_channels())),
            includes=[
                "torbot.gitlab",
                "torbot.http",
            ],
        )

        super().__init__(**bot_config)

    def get_ssl_context(self):
        context = super().get_ssl_context()

        context.verify_mode = ssl.CERT_REQUIRED
        context.load_cert_chain(certfile=self._config.irc_server_tls_auth_cert(),
                                keyfile=self._config.irc_server_tls_auth_key())

        return context


def run_bot_forever(config):
    if not config.irc_enabled():
        return

    bot = TorBot(config)
    bot.run(forever=True)
